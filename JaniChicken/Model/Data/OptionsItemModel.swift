//
//  OptionsItemModel.swift
//  Food Delivery
//
//  Created by Akram Hussain on 15/02/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation
import OrderedDictionary

struct OptionsItemModel {

    var optionsAndPrice = OrderedDictionary<String,String>()
}
