//
//  NotesBody.swift
//  Food Delivery
//
//  Created by Akram Hussain on 07/02/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation

struct NotesBody: Codable {
    let note: String?
}
