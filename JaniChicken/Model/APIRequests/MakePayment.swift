//
//  MakePayment.swift
//  Food Delivery
//
//  Created by Akram Hussain on 06/01/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation

struct MakePayment: Codable{
    let token, orderDescription: String?
    let amount: Int?
    let currencyCode: String?
}
