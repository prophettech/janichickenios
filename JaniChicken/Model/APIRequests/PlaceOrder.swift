//
//  PlaceOrder.swift
//  Food Delivery
//
//  Created by Akram Hussain on 08/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

struct PlaceOrder: Codable {
    let paymentMethod, paymentMethodTitle: String?
    let setPaid: Bool?
    let billing, shipping: Ing?
    let lineItems: [LineItem]?
    let shippingLines: [ShippingLines]?

    enum CodingKeys: String, CodingKey {
        case paymentMethod = "payment_method"
        case paymentMethodTitle = "payment_method_title"
        case setPaid = "set_paid"
        case billing, shipping
        case lineItems = "line_items"
        case shippingLines = "shipping_lines"
    }
}

struct Ing: Codable {
    let firstName, lastName, address1, address2: String?
    let city, state, postcode, country: String?
    let email, phone: String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case address1 = "address_1"
        case address2 = "address_2"
        case city, state, postcode, country, email, phone
    }
}

struct LineItem: Codable {
      let name: String?
      let productID, variationID, quantity: Int?
      let taxClass, subtotal, subtotalTax, total: String?
      let totalTax: String?
      let taxes: [OrderTax]?
      let metaData: [OrderMetaDatum]?
      let sku: String?
      let price: Int?

      enum CodingKeys: String, CodingKey {
          case name
          case productID = "product_id"
          case variationID = "variation_id"
          case quantity
          case taxClass = "tax_class"
          case subtotal
          case subtotalTax = "subtotal_tax"
          case total
          case totalTax = "total_tax"
          case taxes
          case metaData = "meta_data"
          case sku, price
      }
}

struct OrderMetaDatum: Codable {
    let id: Int?
    let key, value: String?
}

struct OrderTax: Codable {
    let id: Int?
    let total, subtotal: String?
}

struct ShippingLines: Codable {
    let methodID, methodTitle: String?
    let total: String?

    enum CodingKeys: String, CodingKey {
        case methodID = "method_id"
        case methodTitle = "method_title"
        case total
    }
}
