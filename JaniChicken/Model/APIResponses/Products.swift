//
//  Products.swift
//  Food Delivery
//
//  Created by Akram Hussain on 30/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

struct Product: Codable {
    let id: Int?
    let name, slug: String?
    let permalink: String?
    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String?
    //let type: TypeEnum?
    let status: Status?
    let featured: Bool?
    //let catalogVisibility: CatalogVisibility?
    let productDescription, shortDescription, sku, price: String?
    let regularPrice, salePrice: String?
    let dateOnSaleFrom, dateOnSaleFromGmt, dateOnSaleTo, dateOnSaleToGmt: JSONNull?
    let priceHTML: String?
    let onSale, purchasable: Bool?
    let totalSales: Int?
    let virtual, downloadable: Bool?
    let downloads: [JSONAny]?
    let downloadLimit, downloadExpiry: Int?
    let externalURL, buttonText: String?
    let taxStatus: TaxStatus?
    let taxClass: String?
    let manageStock: Bool?
    let stockQuantity: JSONNull?
    let stockStatus: StockStatus?
    let backorders: Backorders?
    let backordersAllowed, backordered, soldIndividually: Bool?
    let weight: String?
    let dimensions: Dimensions?
    let shippingRequired, shippingTaxable: Bool?
    let shippingClass: String?
    let shippingClassID: Int?
    let reviewsAllowed: Bool?
    let averageRating: String?
    let ratingCount: Int?
    //let relatedIDS: [Int]?
    //let upsellIDS, crossSellIDS: [JSONAny]?
    let parentID: Int?
    let purchaseNote:  String?
    let images: [Image]?
    //let categories: [ProductCategory]?
    //let tags, images, attributes, defaultAttributes: [JSONAny]?
    //let variations, groupedProducts: [JSONAny]?
    let menuOrder: Int
    //let metaData: [MetaDatum]?
    //let links: ProductLinks?

    enum CodingKeys: String, CodingKey {
        case id, name, slug, permalink
        case dateCreated = "date_created"
        case dateCreatedGmt = "date_created_gmt"
        case dateModified = "date_modified"
        case dateModifiedGmt = "date_modified_gmt"
        //case type
        case status, featured
        //case catalogVisibility = "catalog_visibility"
        case productDescription = "description"
        case shortDescription = "short_description"
        case sku, price
        case regularPrice = "regular_price"
        case salePrice = "sale_price"
        case dateOnSaleFrom = "date_on_sale_from"
        case dateOnSaleFromGmt = "date_on_sale_from_gmt"
        case dateOnSaleTo = "date_on_sale_to"
        case dateOnSaleToGmt = "date_on_sale_to_gmt"
        case priceHTML = "price_html"
        case onSale = "on_sale"
        case purchasable
        case totalSales = "total_sales"
        case virtual, downloadable, downloads
        case downloadLimit = "download_limit"
        case downloadExpiry = "download_expiry"
        case externalURL = "external_url"
        case buttonText = "button_text"
        case taxStatus = "tax_status"
        case taxClass = "tax_class"
        case manageStock = "manage_stock"
        case stockQuantity = "stock_quantity"
        case stockStatus = "stock_status"
        case backorders
        case backordersAllowed = "backorders_allowed"
        case backordered
        case soldIndividually = "sold_individually"
        case weight, dimensions
        case shippingRequired = "shipping_required"
        case shippingTaxable = "shipping_taxable"
        case shippingClass = "shipping_class"
        case shippingClassID = "shipping_class_id"
        case reviewsAllowed = "reviews_allowed"
        case averageRating = "average_rating"
        case ratingCount = "rating_count"
        //case relatedIDS = "related_ids"
        //case upsellIDS = "upsell_ids"
        //case crossSellIDS = "cross_sell_ids"
        case parentID = "parent_id"
        case purchaseNote = "purchase_note"
        case images
//        case categories, tags, images, attributes
//        case defaultAttributes = "default_attributes"
        //case variations
        //case groupedProducts = "grouped_products"
        case menuOrder = "menu_order"
        //case metaData = "meta_data"
        //case links = "_links"
    }
}

enum Backorders: String, Codable {
    case no = "no"
}

enum CatalogVisibility: String, Codable {
    case visible = "visible"
}

struct ProductCategory: Codable {
    let id: Int?
    let name: Name?
    let slug: Slug?
}

enum Name: String, Codable {
    case burgers = "Burgers"
}

enum Slug: String, Codable {
    case burgers = "burgers"
}

struct Dimensions: Codable {
    let length, width, height: String?
}

struct ProductLinks: Codable {
    let linksSelf, collection: [ProductCollection]?

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection
    }
}

struct ProductCollection: Codable {
    let href: String?
}

struct MetaDatum: Codable {
    let id: Int?
    let key: Key?
    let value: ValueUnion?
}

enum Key: String, Codable {
    case tmMeta = "tm_meta"
    case tmMetaCpf = "tm_meta_cpf"
    case wfgSingleGiftAllowed = "_wfg_single_gift_allowed"
    case yoastWpseoContentScore = "_yoast_wpseo_content_score"
    case yoastWpseoPrimaryProductCat = "_yoast_wpseo_primary_product_cat"
}

enum ValueUnion: Codable {
    case string(String)
    case valueClass(ValueClass)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(ValueClass.self) {
            self = .valueClass(x)
            return
        }
        throw DecodingError.typeMismatch(ValueUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ValueUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .valueClass(let x):
            try container.encode(x)
        }
    }
}

struct ValueClass: Codable {
    let mode: Mode?
    let overrideDisplay, overrideFinalTotalBox: String?
    let tmfbuilder: Tmfbuilder?

    enum CodingKeys: String, CodingKey {
        case mode
        case overrideDisplay = "override_display"
        case overrideFinalTotalBox = "override_final_total_box"
        case tmfbuilder
    }
}

enum Mode: String, Codable {
    case builder = "builder"
}

struct Tmfbuilder: Codable {
    let sectionHeaderSize, sectionHeaderTitle, sectionHeaderTitlePosition, sectionHeaderTitleColor: [String]?
    let sectionHeaderSubtitle, sectionHeaderSubtitlePosition, sectionHeaderSubtitleColor: [String]?
    let sectionDividerType: [DividerType]?
    let sections, sectionsSlides: [String]?
    let sectionsSize: [Size]?
    let sectionsUniqid, sectionsStyle: [String]?
    let sectionsPlacement: [SectionsPlacement]?
    let sectionsType, sectionsClass: [String]?
    let sectionsClogic: [SectionsClogic]?
    let sectionsLogic: [String]?
    let sectionsInternalName: [SectionsInternalName]?
    let elementType: [ElementType]?
    let divSize: [Size]?
    let selectboxInternalName: [SelectboxInternalName]?
    let selectboxHeaderSize, selectboxHeaderTitle, selectboxHeaderTitlePosition, selectboxHeaderTitleColor: [String]?
    let selectboxHeaderSubtitle, selectboxHeaderSubtitlePosition, selectboxHeaderSubtitleColor: [String]?
    let selectboxDividerType: [DividerType]?
    let selectboxEnabled, selectboxRequired, selectboxTextBeforePrice, selectboxTextAfterPrice: [String]?
    let selectboxPriceType, selectboxHideAmount, selectboxQuantity, selectboxQuantityMin: [String]?
    let selectboxQuantityMax, selectboxQuantityStep, selectboxQuantityDefaultValue, selectboxPlaceholder: [String]?
    let selectboxUseURL, selectboxChangesProductImage: [String]?
    let multipleSelectboxOptionsTitle: [[MultipleSelectboxOptions]]?
    let multipleSelectboxOptionsImage, multipleSelectboxOptionsImagec, multipleSelectboxOptionsImagep, multipleSelectboxOptionsImagel: [[String]]?
    let multipleSelectboxOptionsValue: [[MultipleSelectboxOptions]]?
    let multipleSelectboxOptionsPrice, multipleSelectboxOptionsSalePrice, multipleSelectboxOptionsPriceType, multipleSelectboxOptionsDescription: [[String]]?
    let multipleSelectboxOptionsURL: [[String]]?
    let selectboxUniqid, selectboxClogic, selectboxLogic, selectboxClass: [String]?
    let selectboxContainerID, selectboxIncludeTaxForFeePriceType, selectboxTaxClassForFeePriceType, selectboxHideElementLabelInCart: [String]?
    let selectboxHideElementValueInCart, selectboxHideElementLabelInOrder, selectboxHideElementValueInOrder, selectboxHideElementLabelInFloatbox: [String]?
    let selectboxHideElementValueInFloatbox: [String]?
    let checkboxesInternalName: [CheckboxesInternalName]?
    let checkboxesHeaderSize, checkboxesHeaderTitle, checkboxesHeaderTitlePosition, checkboxesHeaderTitleColor: [String]?
    let checkboxesHeaderSubtitle, checkboxesHeaderSubtitlePosition, checkboxesHeaderSubtitleColor: [String]?
    let checkboxesDividerType: [DividerType]?
    let checkboxesEnabled, checkboxesRequired, checkboxesTextBeforePrice, checkboxesTextAfterPrice: [String]?
    let checkboxesHideAmount, checkboxesQuantity, checkboxesQuantityMin, checkboxesQuantityMax: [String]?
    let checkboxesQuantityStep, checkboxesQuantityDefaultValue, checkboxesLimitChoices, checkboxesExactlimitChoices: [String]?
    let checkboxesMinimumlimitChoices, checkboxesUseImages, checkboxesUseLightbox, checkboxesSwatchmode: [String]?
    let checkboxesUseColors, checkboxesChangesProductImage, checkboxesItemsPerRow, checkboxesItemsPerRowTablets: [String]?
    let checkboxesItemsPerRowTabletsSmall, checkboxesItemsPerRowSmartphones, checkboxesItemsPerRowIphone5, checkboxesItemsPerRowIphone6: [String]?
    let checkboxesItemsPerRowIphone6Plus, checkboxesItemsPerRowSamsungGalaxy, checkboxesItemsPerRowTabletsGalaxy: [String]?
    let multipleCheckboxesOptionsTitle, multipleCheckboxesOptionsImage, multipleCheckboxesOptionsImagec, multipleCheckboxesOptionsImagep: [[String]]?
    let multipleCheckboxesOptionsImagel, multipleCheckboxesOptionsColor, multipleCheckboxesOptionsValue, multipleCheckboxesOptionsPrice: [[String]]?
    let multipleCheckboxesOptionsSalePrice, multipleCheckboxesOptionsPriceType, multipleCheckboxesOptionsDescription, multipleCheckboxesOptionsURL: [[String]]?
    let checkboxesUniqid, checkboxesClogic, checkboxesLogic, checkboxesClass: [String]?
    let checkboxesContainerID, checkboxesIncludeTaxForFeePriceType, checkboxesTaxClassForFeePriceType, checkboxesHideElementLabelInCart: [String]?
    let checkboxesHideElementValueInCart, checkboxesHideElementLabelInOrder, checkboxesHideElementValueInOrder, checkboxesHideElementLabelInFloatbox: [String]?
    let checkboxesHideElementValueInFloatbox: [String]?

    enum CodingKeys: String, CodingKey {
        case sectionHeaderSize = "section_header_size"
        case sectionHeaderTitle = "section_header_title"
        case sectionHeaderTitlePosition = "section_header_title_position"
        case sectionHeaderTitleColor = "section_header_title_color"
        case sectionHeaderSubtitle = "section_header_subtitle"
        case sectionHeaderSubtitlePosition = "section_header_subtitle_position"
        case sectionHeaderSubtitleColor = "section_header_subtitle_color"
        case sectionDividerType = "section_divider_type"
        case sections
        case sectionsSlides = "sections_slides"
        case sectionsSize = "sections_size"
        case sectionsUniqid = "sections_uniqid"
        case sectionsStyle = "sections_style"
        case sectionsPlacement = "sections_placement"
        case sectionsType = "sections_type"
        case sectionsClass = "sections_class"
        case sectionsClogic = "sections_clogic"
        case sectionsLogic = "sections_logic"
        case sectionsInternalName = "sections_internal_name"
        case elementType = "element_type"
        case divSize = "div_size"
        case selectboxInternalName = "selectbox_internal_name"
        case selectboxHeaderSize = "selectbox_header_size"
        case selectboxHeaderTitle = "selectbox_header_title"
        case selectboxHeaderTitlePosition = "selectbox_header_title_position"
        case selectboxHeaderTitleColor = "selectbox_header_title_color"
        case selectboxHeaderSubtitle = "selectbox_header_subtitle"
        case selectboxHeaderSubtitlePosition = "selectbox_header_subtitle_position"
        case selectboxHeaderSubtitleColor = "selectbox_header_subtitle_color"
        case selectboxDividerType = "selectbox_divider_type"
        case selectboxEnabled = "selectbox_enabled"
        case selectboxRequired = "selectbox_required"
        case selectboxTextBeforePrice = "selectbox_text_before_price"
        case selectboxTextAfterPrice = "selectbox_text_after_price"
        case selectboxPriceType = "selectbox_price_type"
        case selectboxHideAmount = "selectbox_hide_amount"
        case selectboxQuantity = "selectbox_quantity"
        case selectboxQuantityMin = "selectbox_quantity_min"
        case selectboxQuantityMax = "selectbox_quantity_max"
        case selectboxQuantityStep = "selectbox_quantity_step"
        case selectboxQuantityDefaultValue = "selectbox_quantity_default_value"
        case selectboxPlaceholder = "selectbox_placeholder"
        case selectboxUseURL = "selectbox_use_url"
        case selectboxChangesProductImage = "selectbox_changes_product_image"
        case multipleSelectboxOptionsTitle = "multiple_selectbox_options_title"
        case multipleSelectboxOptionsImage = "multiple_selectbox_options_image"
        case multipleSelectboxOptionsImagec = "multiple_selectbox_options_imagec"
        case multipleSelectboxOptionsImagep = "multiple_selectbox_options_imagep"
        case multipleSelectboxOptionsImagel = "multiple_selectbox_options_imagel"
        case multipleSelectboxOptionsValue = "multiple_selectbox_options_value"
        case multipleSelectboxOptionsPrice = "multiple_selectbox_options_price"
        case multipleSelectboxOptionsSalePrice = "multiple_selectbox_options_sale_price"
        case multipleSelectboxOptionsPriceType = "multiple_selectbox_options_price_type"
        case multipleSelectboxOptionsDescription = "multiple_selectbox_options_description"
        case multipleSelectboxOptionsURL = "multiple_selectbox_options_url"
        case selectboxUniqid = "selectbox_uniqid"
        case selectboxClogic = "selectbox_clogic"
        case selectboxLogic = "selectbox_logic"
        case selectboxClass = "selectbox_class"
        case selectboxContainerID = "selectbox_container_id"
        case selectboxIncludeTaxForFeePriceType = "selectbox_include_tax_for_fee_price_type"
        case selectboxTaxClassForFeePriceType = "selectbox_tax_class_for_fee_price_type"
        case selectboxHideElementLabelInCart = "selectbox_hide_element_label_in_cart"
        case selectboxHideElementValueInCart = "selectbox_hide_element_value_in_cart"
        case selectboxHideElementLabelInOrder = "selectbox_hide_element_label_in_order"
        case selectboxHideElementValueInOrder = "selectbox_hide_element_value_in_order"
        case selectboxHideElementLabelInFloatbox = "selectbox_hide_element_label_in_floatbox"
        case selectboxHideElementValueInFloatbox = "selectbox_hide_element_value_in_floatbox"
        case checkboxesInternalName = "checkboxes_internal_name"
        case checkboxesHeaderSize = "checkboxes_header_size"
        case checkboxesHeaderTitle = "checkboxes_header_title"
        case checkboxesHeaderTitlePosition = "checkboxes_header_title_position"
        case checkboxesHeaderTitleColor = "checkboxes_header_title_color"
        case checkboxesHeaderSubtitle = "checkboxes_header_subtitle"
        case checkboxesHeaderSubtitlePosition = "checkboxes_header_subtitle_position"
        case checkboxesHeaderSubtitleColor = "checkboxes_header_subtitle_color"
        case checkboxesDividerType = "checkboxes_divider_type"
        case checkboxesEnabled = "checkboxes_enabled"
        case checkboxesRequired = "checkboxes_required"
        case checkboxesTextBeforePrice = "checkboxes_text_before_price"
        case checkboxesTextAfterPrice = "checkboxes_text_after_price"
        case checkboxesHideAmount = "checkboxes_hide_amount"
        case checkboxesQuantity = "checkboxes_quantity"
        case checkboxesQuantityMin = "checkboxes_quantity_min"
        case checkboxesQuantityMax = "checkboxes_quantity_max"
        case checkboxesQuantityStep = "checkboxes_quantity_step"
        case checkboxesQuantityDefaultValue = "checkboxes_quantity_default_value"
        case checkboxesLimitChoices = "checkboxes_limit_choices"
        case checkboxesExactlimitChoices = "checkboxes_exactlimit_choices"
        case checkboxesMinimumlimitChoices = "checkboxes_minimumlimit_choices"
        case checkboxesUseImages = "checkboxes_use_images"
        case checkboxesUseLightbox = "checkboxes_use_lightbox"
        case checkboxesSwatchmode = "checkboxes_swatchmode"
        case checkboxesUseColors = "checkboxes_use_colors"
        case checkboxesChangesProductImage = "checkboxes_changes_product_image"
        case checkboxesItemsPerRow = "checkboxes_items_per_row"
        case checkboxesItemsPerRowTablets = "checkboxes_items_per_row_tablets"
        case checkboxesItemsPerRowTabletsSmall = "checkboxes_items_per_row_tablets_small"
        case checkboxesItemsPerRowSmartphones = "checkboxes_items_per_row_smartphones"
        case checkboxesItemsPerRowIphone5 = "checkboxes_items_per_row_iphone5"
        case checkboxesItemsPerRowIphone6 = "checkboxes_items_per_row_iphone6"
        case checkboxesItemsPerRowIphone6Plus = "checkboxes_items_per_row_iphone6_plus"
        case checkboxesItemsPerRowSamsungGalaxy = "checkboxes_items_per_row_samsung_galaxy"
        case checkboxesItemsPerRowTabletsGalaxy = "checkboxes_items_per_row_tablets_galaxy"
        case multipleCheckboxesOptionsTitle = "multiple_checkboxes_options_title"
        case multipleCheckboxesOptionsImage = "multiple_checkboxes_options_image"
        case multipleCheckboxesOptionsImagec = "multiple_checkboxes_options_imagec"
        case multipleCheckboxesOptionsImagep = "multiple_checkboxes_options_imagep"
        case multipleCheckboxesOptionsImagel = "multiple_checkboxes_options_imagel"
        case multipleCheckboxesOptionsColor = "multiple_checkboxes_options_color"
        case multipleCheckboxesOptionsValue = "multiple_checkboxes_options_value"
        case multipleCheckboxesOptionsPrice = "multiple_checkboxes_options_price"
        case multipleCheckboxesOptionsSalePrice = "multiple_checkboxes_options_sale_price"
        case multipleCheckboxesOptionsPriceType = "multiple_checkboxes_options_price_type"
        case multipleCheckboxesOptionsDescription = "multiple_checkboxes_options_description"
        case multipleCheckboxesOptionsURL = "multiple_checkboxes_options_url"
        case checkboxesUniqid = "checkboxes_uniqid"
        case checkboxesClogic = "checkboxes_clogic"
        case checkboxesLogic = "checkboxes_logic"
        case checkboxesClass = "checkboxes_class"
        case checkboxesContainerID = "checkboxes_container_id"
        case checkboxesIncludeTaxForFeePriceType = "checkboxes_include_tax_for_fee_price_type"
        case checkboxesTaxClassForFeePriceType = "checkboxes_tax_class_for_fee_price_type"
        case checkboxesHideElementLabelInCart = "checkboxes_hide_element_label_in_cart"
        case checkboxesHideElementValueInCart = "checkboxes_hide_element_value_in_cart"
        case checkboxesHideElementLabelInOrder = "checkboxes_hide_element_label_in_order"
        case checkboxesHideElementValueInOrder = "checkboxes_hide_element_value_in_order"
        case checkboxesHideElementLabelInFloatbox = "checkboxes_hide_element_label_in_floatbox"
        case checkboxesHideElementValueInFloatbox = "checkboxes_hide_element_value_in_floatbox"
    }
}

enum DividerType: String, Codable {
    case none = "none"
}

enum CheckboxesInternalName: String, Codable {
    case checkboxes = "Checkboxes"
}

enum Size: String, Codable {
    case w100 = "w100"
}

enum ElementType: String, Codable {
    case checkboxes = "checkboxes"
    case selectbox = "selectbox"
}

enum MultipleSelectboxOptions: String, Codable {
    case coke = "Coke"
    case dietCoke = "Diet Coke"
    case drPepper = "Dr Pepper"
    case fanta = "Fanta"
    case sprite = "Sprite"
    case the12 = "1/2"
    case the14 = "1/4"
}

enum SectionsClogic: String, Codable {
    case toggleShowWhatAnyRules = "{\"toggle\":\"show\",\"what\":\"any\",\"rules\":[]}"
}

enum SectionsInternalName: String, Codable {
    case section = "Section"
}

enum SectionsPlacement: String, Codable {
    case before = "before"
}

enum SelectboxInternalName: String, Codable {
    case selectBox = "Select Box"
}

enum Status: String, Codable {
    case publish = "publish"
}

enum StockStatus: String, Codable {
    case instock = "instock"
}

enum TaxStatus: String, Codable {
    case taxable = "taxable"
}

enum TypeEnum: String, Codable {
    case simple = "simple"
}

typealias Products = [Product]

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
