//
//  Categories.swift
//  Food Delivery
//
//  Created by Akram Hussain on 01/12/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: Int?
    let name, slug: String?
    let parent: Int?
    let categoryDescription: String?
    let display: Display?
    let image: Image?
    let menuOrder, count: Int
    let links: Links?

    enum CodingKeys: String, CodingKey {
        case id, name, slug, parent
        case categoryDescription = "description"
        case display, image
        case menuOrder = "menu_order"
        case count
        case links = "_links"
    }
}

enum Display: String, Codable {
    case displayDefault = "default"
    case products = "products"
}

// MARK: - Image
struct Image: Codable {
    let id: Int?
    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String?
    let src: String?
    let name, alt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case dateCreated = "date_created"
        case dateCreatedGmt = "date_created_gmt"
        case dateModified = "date_modified"
        case dateModifiedGmt = "date_modified_gmt"
        case src, name, alt
    }
}

struct Links: Codable {
    let linksSelf, collection, up: [Collection]?

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection, up
    }
}

struct Collection: Codable {
    let href: String?
}

typealias Categories = [Category]
