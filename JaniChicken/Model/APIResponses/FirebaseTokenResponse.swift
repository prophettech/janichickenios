//
//  FirebaseTokenResponse.swift
//  Food Delivery
//
//  Created by Akram Hussain on 05/04/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation

struct FirebaseTokenResponse: Codable {
    let success: Int
}
