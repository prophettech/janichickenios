//
//  WorldpayResponse.swift
//  Food Delivery
//
//  Created by Akram Hussain on 06/01/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation

struct WorlpayResponse: Codable {
    let orderCode, token, orderDescription: String?
    let amount: Int?
    let currencyCode, paymentStatus: String?
    let paymentResponse: PaymentResponse?
    let environment: String?
}

struct PaymentResponse: Codable {
    let type, name: String?
    let expiryMonth, expiryYear: Int?
    let cardType, maskedCardNumber: String?
}
