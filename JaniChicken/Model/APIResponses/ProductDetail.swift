//
//  ProductDetail.swift
//  Food Delivery
//
//  Created by Akram Hussain on 03/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

struct ProductDetail: Codable {
    let id : Int?
    let name : String?
    let slug : String?
    let permalink : String?
    let date_created : String?
    let date_created_gmt : String?
    let date_modified : String?
    let date_modified_gmt : String?
    let type : String?
    let status : String?
    let featured : Bool?
    let catalog_visibility : String?
    let description : String?
    let short_description : String?
    let sku : String?
    let price : String?
    let regular_price : String?
    let sale_price : String?
    let date_on_sale_from : String?
    let date_on_sale_from_gmt : String?
    let date_on_sale_to : String?
    let date_on_sale_to_gmt : String?
    let price_html : String?
    let on_sale : Bool?
    let purchasable : Bool?
    let total_sales : Int?
    let virtual : Bool?
    let downloadable : Bool?
    //let downloads : [String]?
    let download_limit : Int?
    let download_expiry : Int?
    let external_url : String?
    let button_text : String?
    let tax_status : String?
    let tax_class : String?
    let manage_stock : Bool?
    let stock_quantity : String?
    let stock_status : String?
    let backorders : String?
    let backorders_allowed : Bool?
    let backordered : Bool?
    let sold_individually : Bool?
    let weight : String?
    let dimensions : DetailDimensions?
    let shipping_required : Bool?
    let shipping_taxable : Bool?
    let shipping_class : String?
    let shipping_class_id : Int?
    let reviews_allowed : Bool?
    let average_rating : String?
    let rating_count : Int?
   // let related_ids : [Int]?
   // let upsell_ids : [String]?
   // let cross_sell_ids : [String]?
    let parent_id : Int?
    let purchase_note : String?
    let categories : [DetailCategories]?
    //let tags : [String]?
    let images : [Image]?
    //let attributes : [String]?
    //let default_attributes : [String]?
   // let variations : [String]?
  //  let grouped_products : [String]?
    let menu_order : Int?
    let meta_data : [Meta_data]?
    let _links : Detaillinks?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case slug = "slug"
        case permalink = "permalink"
        case date_created = "date_created"
        case date_created_gmt = "date_created_gmt"
        case date_modified = "date_modified"
        case date_modified_gmt = "date_modified_gmt"
        case type = "type"
        case status = "status"
        case featured = "featured"
        case catalog_visibility = "catalog_visibility"
        case description = "description"
        case short_description = "short_description"
        case sku = "sku"
        case price = "price"
        case regular_price = "regular_price"
        case sale_price = "sale_price"
        case date_on_sale_from = "date_on_sale_from"
        case date_on_sale_from_gmt = "date_on_sale_from_gmt"
        case date_on_sale_to = "date_on_sale_to"
        case date_on_sale_to_gmt = "date_on_sale_to_gmt"
        case price_html = "price_html"
        case on_sale = "on_sale"
        case purchasable = "purchasable"
        case total_sales = "total_sales"
        case virtual = "virtual"
        case downloadable = "downloadable"
        //case downloads = "downloads"
        case download_limit = "download_limit"
        case download_expiry = "download_expiry"
        case external_url = "external_url"
        case button_text = "button_text"
        case tax_status = "tax_status"
        case tax_class = "tax_class"
        case manage_stock = "manage_stock"
        case stock_quantity = "stock_quantity"
        case stock_status = "stock_status"
        case backorders = "backorders"
        case backorders_allowed = "backorders_allowed"
        case backordered = "backordered"
        case sold_individually = "sold_individually"
        case weight = "weight"
        case dimensions = "dimensions"
        case shipping_required = "shipping_required"
        case shipping_taxable = "shipping_taxable"
        case shipping_class = "shipping_class"
        case shipping_class_id = "shipping_class_id"
        case reviews_allowed = "reviews_allowed"
        case average_rating = "average_rating"
        case rating_count = "rating_count"
//        case related_ids = "related_ids"
//        case upsell_ids = "upsell_ids"
//        case cross_sell_ids = "cross_sell_ids"
        case parent_id = "parent_id"
        case purchase_note = "purchase_note"
        case categories = "categories"
        //case tags = "tags"
        case images = "images"
        //case attributes = "attributes"
//        case default_attributes = "default_attributes"
//        case variations = "variations"
//        case grouped_products = "grouped_products"
        case menu_order = "menu_order"
        case meta_data = "meta_data"
        case _links = "_links"
    }
    
    struct Image: Codable {
        let id: Int
        let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String
        let src: String
        let name, alt: String

        enum CodingKeys: String, CodingKey {
            case id
            case dateCreated = "date_created"
            case dateCreatedGmt = "date_created_gmt"
            case dateModified = "date_modified"
            case dateModifiedGmt = "date_modified_gmt"
            case src, name, alt
        }
    }
    
    struct Meta_data : Codable {
        let id : Int?
        let key : String?
        let value : Value?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case key = "key"
            case value = "value"
        }
    }
        
    struct Detaillinks : Codable {
        let detailSelf : [DetailCollection]?
        let collection : [DetailCollection]?
        
        enum CodingKeys: String, CodingKey {
            
            case detailSelf = "self"
            case collection = "collection"
        }
    }
    
    struct DetailCollection : Codable {
        let href : String?
        
        enum CodingKeys: String, CodingKey {
            case href = "href"
        }
    }
    
    enum Value: Codable {
        case string(String)
        case valueClass(ValueClass)

        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            if let x = try? container.decode(String.self) {
                self = .string(x)
                return
            }
            if let x = try? container.decode(ValueClass.self) {
                self = .valueClass(x)
                return
            }
            throw DecodingError.typeMismatch(ValueUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ValueUnion"))
        }

        func encode(to encoder: Encoder) throws {
            var container = encoder.singleValueContainer()
            switch self {
            case .string(let x):
                try container.encode(x)
            case .valueClass(let x):
                try container.encode(x)
            }
        }
    }
    
    struct ValueClass: Codable {
        let mode, overrideDisplay, overrideFinalTotalBox: String?
        let tmfbuilder: Tmfbuilder?

        enum CodingKeys: String, CodingKey {
            case mode
            case overrideDisplay = "override_display"
            case overrideFinalTotalBox = "override_final_total_box"
            case tmfbuilder
        }
    }
    
    
    struct DetailCategories : Codable {
        let id : Int?
        let name : String?
        let slug : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case name = "name"
            case slug = "slug"
        }
    }
    
    struct DetailDimensions : Codable {
        let length : String?
        let width : String?
        let height : String?
        
        enum CodingKeys: String, CodingKey {
            
            case length = "length"
            case width = "width"
            case height = "height"
        }
    }
    
    struct Tmfbuilder: Codable {
        let sectionHeaderSize, sectionHeaderTitle, sectionHeaderTitlePosition, sectionHeaderTitleColor: [String]?
        let sectionHeaderSubtitle, sectionHeaderSubtitlePosition, sectionHeaderSubtitleColor, sectionDividerType: [String]?
        let sections, sectionsSlides, sectionsSize, sectionsUniqid: [String]?
        let sectionsStyle, sectionsPlacement, sectionsType, sectionsClass: [String]?
        let sectionsClogic, sectionsLogic, sectionsInternalName, elementType: [String]?
        let divSize, selectboxInternalName, selectboxHeaderSize, selectboxHeaderTitle: [String]?
        let selectboxHeaderTitlePosition, selectboxHeaderTitleColor, selectboxHeaderSubtitle, selectboxHeaderSubtitlePosition: [String]?
        let selectboxHeaderSubtitleColor, selectboxDividerType, selectboxEnabled, selectboxRequired: [String]?
        let selectboxTextBeforePrice, selectboxTextAfterPrice, selectboxPriceType, selectboxHideAmount: [String]?
        let selectboxQuantity, selectboxQuantityMin, selectboxQuantityMax, selectboxQuantityStep: [String]?
        let selectboxQuantityDefaultValue, selectboxPlaceholder, selectboxUseURL, selectboxChangesProductImage: [String]?
        let multipleSelectboxOptionsTitle, multipleSelectboxOptionsImage, multipleSelectboxOptionsImagec, multipleSelectboxOptionsImagep: [[String]]?
        let multipleSelectboxOptionsImagel, multipleSelectboxOptionsValue, multipleSelectboxOptionsPrice, multipleSelectboxOptionsSalePrice: [[String]]?
        let multipleSelectboxOptionsPriceType, multipleSelectboxOptionsDescription, multipleSelectboxOptionsURL: [[String]]?
        let selectboxUniqid, selectboxClogic, selectboxLogic, selectboxClass: [String]?
        let selectboxContainerID, selectboxIncludeTaxForFeePriceType, selectboxTaxClassForFeePriceType, selectboxHideElementLabelInCart: [String]?
        let selectboxHideElementValueInCart, selectboxHideElementLabelInOrder, selectboxHideElementValueInOrder, selectboxHideElementLabelInFloatbox: [String]?
        let selectboxHideElementValueInFloatbox, checkboxesInternalName, checkboxesHeaderSize, checkboxesHeaderTitle: [String]?
        let checkboxesHeaderTitlePosition, checkboxesHeaderTitleColor, checkboxesHeaderSubtitle, checkboxesHeaderSubtitlePosition: [String]?
        let checkboxesHeaderSubtitleColor, checkboxesDividerType, checkboxesEnabled, checkboxesRequired: [String]?
        let checkboxesTextBeforePrice, checkboxesTextAfterPrice, checkboxesHideAmount, checkboxesQuantity: [String]?
        let checkboxesQuantityMin, checkboxesQuantityMax, checkboxesQuantityStep, checkboxesQuantityDefaultValue: [String]?
        let checkboxesLimitChoices, checkboxesExactlimitChoices, checkboxesMinimumlimitChoices, checkboxesUseImages: [String]?
        let checkboxesUseLightbox, checkboxesSwatchmode, checkboxesUseColors, checkboxesChangesProductImage: [String]?
        let checkboxesItemsPerRow, checkboxesItemsPerRowTablets, checkboxesItemsPerRowTabletsSmall, checkboxesItemsPerRowSmartphones: [String]?
        let checkboxesItemsPerRowIphone5, checkboxesItemsPerRowIphone6, checkboxesItemsPerRowIphone6Plus, checkboxesItemsPerRowSamsungGalaxy: [String]?
        let checkboxesItemsPerRowTabletsGalaxy: [String]?
        let multipleCheckboxesOptionsTitle, multipleCheckboxesOptionsImage, multipleCheckboxesOptionsImagec, multipleCheckboxesOptionsImagep: [[String]]?
        let multipleCheckboxesOptionsImagel, multipleCheckboxesOptionsColor, multipleCheckboxesOptionsValue, multipleCheckboxesOptionsPrice: [[String]]?
        let multipleCheckboxesOptionsSalePrice, multipleCheckboxesOptionsPriceType, multipleCheckboxesOptionsDescription, multipleCheckboxesOptionsURL: [[String]]?
        let checkboxesUniqid, checkboxesClogic, checkboxesLogic, checkboxesClass: [String]?
        let checkboxesContainerID, checkboxesIncludeTaxForFeePriceType, checkboxesTaxClassForFeePriceType, checkboxesHideElementLabelInCart: [String]?
        let checkboxesHideElementValueInCart, checkboxesHideElementLabelInOrder, checkboxesHideElementValueInOrder, checkboxesHideElementLabelInFloatbox: [String]?
        let checkboxesHideElementValueInFloatbox: [String]?
        
        enum CodingKeys: String, CodingKey {
            case sectionHeaderSize = "section_header_size"
            case sectionHeaderTitle = "section_header_title"
            case sectionHeaderTitlePosition = "section_header_title_position"
            case sectionHeaderTitleColor = "section_header_title_color"
            case sectionHeaderSubtitle = "section_header_subtitle"
            case sectionHeaderSubtitlePosition = "section_header_subtitle_position"
            case sectionHeaderSubtitleColor = "section_header_subtitle_color"
            case sectionDividerType = "section_divider_type"
            case sections
            case sectionsSlides = "sections_slides"
            case sectionsSize = "sections_size"
            case sectionsUniqid = "sections_uniqid"
            case sectionsStyle = "sections_style"
            case sectionsPlacement = "sections_placement"
            case sectionsType = "sections_type"
            case sectionsClass = "sections_class"
            case sectionsClogic = "sections_clogic"
            case sectionsLogic = "sections_logic"
            case sectionsInternalName = "sections_internal_name"
            case elementType = "element_type"
            case divSize = "div_size"
            case selectboxInternalName = "selectbox_internal_name"
            case selectboxHeaderSize = "selectbox_header_size"
            case selectboxHeaderTitle = "selectbox_header_title"
            case selectboxHeaderTitlePosition = "selectbox_header_title_position"
            case selectboxHeaderTitleColor = "selectbox_header_title_color"
            case selectboxHeaderSubtitle = "selectbox_header_subtitle"
            case selectboxHeaderSubtitlePosition = "selectbox_header_subtitle_position"
            case selectboxHeaderSubtitleColor = "selectbox_header_subtitle_color"
            case selectboxDividerType = "selectbox_divider_type"
            case selectboxEnabled = "selectbox_enabled"
            case selectboxRequired = "selectbox_required"
            case selectboxTextBeforePrice = "selectbox_text_before_price"
            case selectboxTextAfterPrice = "selectbox_text_after_price"
            case selectboxPriceType = "selectbox_price_type"
            case selectboxHideAmount = "selectbox_hide_amount"
            case selectboxQuantity = "selectbox_quantity"
            case selectboxQuantityMin = "selectbox_quantity_min"
            case selectboxQuantityMax = "selectbox_quantity_max"
            case selectboxQuantityStep = "selectbox_quantity_step"
            case selectboxQuantityDefaultValue = "selectbox_quantity_default_value"
            case selectboxPlaceholder = "selectbox_placeholder"
            case selectboxUseURL = "selectbox_use_url"
            case selectboxChangesProductImage = "selectbox_changes_product_image"
            case multipleSelectboxOptionsTitle = "multiple_selectbox_options_title"
            case multipleSelectboxOptionsImage = "multiple_selectbox_options_image"
            case multipleSelectboxOptionsImagec = "multiple_selectbox_options_imagec"
            case multipleSelectboxOptionsImagep = "multiple_selectbox_options_imagep"
            case multipleSelectboxOptionsImagel = "multiple_selectbox_options_imagel"
            case multipleSelectboxOptionsValue = "multiple_selectbox_options_value"
            case multipleSelectboxOptionsPrice = "multiple_selectbox_options_price"
            case multipleSelectboxOptionsSalePrice = "multiple_selectbox_options_sale_price"
            case multipleSelectboxOptionsPriceType = "multiple_selectbox_options_price_type"
            case multipleSelectboxOptionsDescription = "multiple_selectbox_options_description"
            case multipleSelectboxOptionsURL = "multiple_selectbox_options_url"
            case selectboxUniqid = "selectbox_uniqid"
            case selectboxClogic = "selectbox_clogic"
            case selectboxLogic = "selectbox_logic"
            case selectboxClass = "selectbox_class"
            case selectboxContainerID = "selectbox_container_id"
            case selectboxIncludeTaxForFeePriceType = "selectbox_include_tax_for_fee_price_type"
            case selectboxTaxClassForFeePriceType = "selectbox_tax_class_for_fee_price_type"
            case selectboxHideElementLabelInCart = "selectbox_hide_element_label_in_cart"
            case selectboxHideElementValueInCart = "selectbox_hide_element_value_in_cart"
            case selectboxHideElementLabelInOrder = "selectbox_hide_element_label_in_order"
            case selectboxHideElementValueInOrder = "selectbox_hide_element_value_in_order"
            case selectboxHideElementLabelInFloatbox = "selectbox_hide_element_label_in_floatbox"
            case selectboxHideElementValueInFloatbox = "selectbox_hide_element_value_in_floatbox"
            case checkboxesInternalName = "checkboxes_internal_name"
            case checkboxesHeaderSize = "checkboxes_header_size"
            case checkboxesHeaderTitle = "checkboxes_header_title"
            case checkboxesHeaderTitlePosition = "checkboxes_header_title_position"
            case checkboxesHeaderTitleColor = "checkboxes_header_title_color"
            case checkboxesHeaderSubtitle = "checkboxes_header_subtitle"
            case checkboxesHeaderSubtitlePosition = "checkboxes_header_subtitle_position"
            case checkboxesHeaderSubtitleColor = "checkboxes_header_subtitle_color"
            case checkboxesDividerType = "checkboxes_divider_type"
            case checkboxesEnabled = "checkboxes_enabled"
            case checkboxesRequired = "checkboxes_required"
            case checkboxesTextBeforePrice = "checkboxes_text_before_price"
            case checkboxesTextAfterPrice = "checkboxes_text_after_price"
            case checkboxesHideAmount = "checkboxes_hide_amount"
            case checkboxesQuantity = "checkboxes_quantity"
            case checkboxesQuantityMin = "checkboxes_quantity_min"
            case checkboxesQuantityMax = "checkboxes_quantity_max"
            case checkboxesQuantityStep = "checkboxes_quantity_step"
            case checkboxesQuantityDefaultValue = "checkboxes_quantity_default_value"
            case checkboxesLimitChoices = "checkboxes_limit_choices"
            case checkboxesExactlimitChoices = "checkboxes_exactlimit_choices"
            case checkboxesMinimumlimitChoices = "checkboxes_minimumlimit_choices"
            case checkboxesUseImages = "checkboxes_use_images"
            case checkboxesUseLightbox = "checkboxes_use_lightbox"
            case checkboxesSwatchmode = "checkboxes_swatchmode"
            case checkboxesUseColors = "checkboxes_use_colors"
            case checkboxesChangesProductImage = "checkboxes_changes_product_image"
            case checkboxesItemsPerRow = "checkboxes_items_per_row"
            case checkboxesItemsPerRowTablets = "checkboxes_items_per_row_tablets"
            case checkboxesItemsPerRowTabletsSmall = "checkboxes_items_per_row_tablets_small"
            case checkboxesItemsPerRowSmartphones = "checkboxes_items_per_row_smartphones"
            case checkboxesItemsPerRowIphone5 = "checkboxes_items_per_row_iphone5"
            case checkboxesItemsPerRowIphone6 = "checkboxes_items_per_row_iphone6"
            case checkboxesItemsPerRowIphone6Plus = "checkboxes_items_per_row_iphone6_plus"
            case checkboxesItemsPerRowSamsungGalaxy = "checkboxes_items_per_row_samsung_galaxy"
            case checkboxesItemsPerRowTabletsGalaxy = "checkboxes_items_per_row_tablets_galaxy"
            case multipleCheckboxesOptionsTitle = "multiple_checkboxes_options_title"
            case multipleCheckboxesOptionsImage = "multiple_checkboxes_options_image"
            case multipleCheckboxesOptionsImagec = "multiple_checkboxes_options_imagec"
            case multipleCheckboxesOptionsImagep = "multiple_checkboxes_options_imagep"
            case multipleCheckboxesOptionsImagel = "multiple_checkboxes_options_imagel"
            case multipleCheckboxesOptionsColor = "multiple_checkboxes_options_color"
            case multipleCheckboxesOptionsValue = "multiple_checkboxes_options_value"
            case multipleCheckboxesOptionsPrice = "multiple_checkboxes_options_price"
            case multipleCheckboxesOptionsSalePrice = "multiple_checkboxes_options_sale_price"
            case multipleCheckboxesOptionsPriceType = "multiple_checkboxes_options_price_type"
            case multipleCheckboxesOptionsDescription = "multiple_checkboxes_options_description"
            case multipleCheckboxesOptionsURL = "multiple_checkboxes_options_url"
            case checkboxesUniqid = "checkboxes_uniqid"
            case checkboxesClogic = "checkboxes_clogic"
            case checkboxesLogic = "checkboxes_logic"
            case checkboxesClass = "checkboxes_class"
            case checkboxesContainerID = "checkboxes_container_id"
            case checkboxesIncludeTaxForFeePriceType = "checkboxes_include_tax_for_fee_price_type"
            case checkboxesTaxClassForFeePriceType = "checkboxes_tax_class_for_fee_price_type"
            case checkboxesHideElementLabelInCart = "checkboxes_hide_element_label_in_cart"
            case checkboxesHideElementValueInCart = "checkboxes_hide_element_value_in_cart"
            case checkboxesHideElementLabelInOrder = "checkboxes_hide_element_label_in_order"
            case checkboxesHideElementValueInOrder = "checkboxes_hide_element_value_in_order"
            case checkboxesHideElementLabelInFloatbox = "checkboxes_hide_element_label_in_floatbox"
            case checkboxesHideElementValueInFloatbox = "checkboxes_hide_element_value_in_floatbox"
        }
    }
}
