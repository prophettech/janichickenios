//
//  TrackOrderViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 25/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON

class TrackOrderViewController : UIViewController {

    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var billingAddressLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    var order: OrderResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    
    func setData(){
        orderNumberLabel.text = order?.number
        dateLabel.text = order?.datePaid
        emailLabel.text = order?.billing?.email
        totalLabel.text = "￡" + (order?.total ?? "")
        billingAddressLabel.text = "\((order?.billing?.address1)! + ", " + (order?.billing?.address2)! + "\n" + (order?.billing?.city)! + "\n" + (order?.billing?.postcode)! + "\n" + (order?.billing?.phone)!)" 
    }
    
    @IBAction func doneActionPressed(_ sender: Any) {
        if self.navigationController?.viewControllers.firstIndex(of: self) != nil {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
