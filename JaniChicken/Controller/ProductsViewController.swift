//
//  ViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 25/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit

class ProductsViewController: UITableViewController {

    @IBOutlet var productsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cartButton: UIBarButtonItem!
    
    let activityIndicatorView = UIActivityIndicatorView()
    var products: Products?
    var filteredProducts: Products?
    var categoryID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllProducts()
    }
    
    func getAllProducts(){
        showActivityIndicator()
        Services.getProductsByCategory(id: categoryID) { (products) in
            self.products = products
            self.products = self.products?.sorted(by: { $0.menuOrder < $1.menuOrder })
            self.productsTableView.dataSource = self
            self.productsTableView.delegate = self
            self.productsTableView.reloadData()
            self.hideActivityIndicator()
        }
    }
    
    func showActivityIndicator()
    {
        activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicatorView.center = productsTableView.center
        if #available(iOS 13.0, *) {
            activityIndicatorView.style = .large
        }
        activityIndicatorView.color = .red
        
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    @IBAction func cartAction(_ sender: Any) {
        let vc : CartDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cart") as! CartDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func hideActivityIndicator()
    {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.text != "" {
            return filteredProducts?.count ?? 0
        }
        return products?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsCell", for: indexPath) as! CategoriesCell
        
        cell.product = products?[indexPath.row]
        if searchBar.text != "" {
            cell.product = self.filteredProducts?[indexPath.row]
        }
        cell.selectionStyle = .gray
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc : MealDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as! MealDetailViewController
        vc.productId = String((products?[indexPath.row].id ?? 0))
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ProductsViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredProducts = self.products?.filter({ (products) -> Bool in
            return products.name?.lowercased().range(of: searchText.lowercased()) != nil
        })
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
