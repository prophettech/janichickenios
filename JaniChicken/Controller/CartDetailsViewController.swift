//
//  CartDetailsViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 09/01/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import UIKit

class CartDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CartItemsDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noItemsLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var checkOutButton: UIButton!
    
    private var quantity = Int()
    private var price = Double()
    private var cart = Cart()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkOutButton.layer.borderWidth = 2.0
        checkOutButton.layer.cornerRadius = 10.0
        checkOutButton.layer.borderColor = UIColor.white.cgColor
        navigationController?.title = "Cart"
        
        setData()
    }
    
    func setData(){
        RealmUtil.getCartItems { (items) in
            print("------", items)
            if (items.products.count > 0){
                self.noItemsLabel.isHidden = true
                self.quantity = 0
                self.price = 0
                let cart = Cart()
                for item in items.products {
                    self.quantity = self.quantity + item.quantity
                    self.price = self.price + item.price
                }
                cart.totalQuantity = self.quantity
                cart.totalPrice = self.price
                cart.products = items.products
                RealmUtil.updateCart(cart: cart)
                self.cart = cart
                self.quantityLabel.text = String(cart.totalQuantity)
                self.priceLabel.text = "￡" + Util.formatNumber(Float(cart.totalPrice))!
                //Util.formatNumber(cart.totalPrice)
                self.tableView.delegate = self
                self.tableView.dataSource = self
            }
        }
    }
    
    @IBAction func checkOutAction(_ sender: Any) {
        if cart.products.count > 0{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "CartVC") as! CartViewController
            self.navigationController?.pushViewController(newViewController, animated: true)
        } else{
            Util.showAlert(vc: self, title: "No Items", msg: "Please add items to your cart!")
        }
    }
    
    
}

extension CartDetailsViewController {
    
    func onAddItem(item: CartItem) {
        var quantity = item.quantity
        let price = item.originalPrice
        if quantity < 99 {
            quantity += 1
            let cartItem = CartItem()
            cartItem.quantity = quantity
            cartItem.price = Double(price * Double(quantity)).rounded(toPlaces: 2)
            cartItem.name = item.name
            cartItem.originalPrice = price
            cartItem.productId = item.productId
            RealmUtil.setCartItem(cartItem: cartItem)
        }
        setData()
        tableView.reloadData()
    }
    
    func onRemoveItem(item: CartItem) {
        var quantity = item.quantity
        let price = item.originalPrice
        if quantity > 0 {
            quantity -= 1
            let cartItem = CartItem()
            if quantity == 0 {
                RealmUtil.deleteItem(cartItem: item)
                setData()
                tableView.reloadData()
                return
            }
            cartItem.quantity = quantity
            cartItem.price = Double(price * Double(quantity)).rounded(toPlaces: 2)
            cartItem.name = item.name
            cartItem.originalPrice = price
            cartItem.productId = item.productId
            RealmUtil.setCartItem(cartItem: cartItem)
        }
        setData()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as! CartTableViewCell
        let item = cart.products[indexPath.row]
        cell.cartItem = item
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.products.count
    }
    
}
