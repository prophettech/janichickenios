//
//  MealDetailViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 25/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown
import OrderedDictionary

class MealDetailViewController : UIViewController
{
    @IBOutlet weak var quantityButtonsContainerView: UIView!
    @IBOutlet weak var mealImageView: UIImageView!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealDecriptionLabel: UILabel!
    @IBOutlet weak var cartBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var optionsPriceLabel: UILabel!
    @IBOutlet weak var typeDropDown: DropDown!
    @IBOutlet weak var drinksDropDown: DropDown!
    @IBOutlet weak var optionsLabel1: UILabel!
    @IBOutlet weak var optionsLabel2: UILabel!
    @IBOutlet weak var optionsSelectedLabel: UITextView!
    @IBOutlet weak var cartButton: UIBarButtonItem!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var optionsLabel3: UILabel!
    @IBOutlet weak var option3DropDown: DropDown!
    @IBOutlet weak var optionLabel4: UILabel!
    @IBOutlet weak var option4DropDown: DropDown!
    @IBOutlet weak var optionLabel5: UILabel!
    @IBOutlet weak var option5DropDown: DropDown!
    
    var tmfBuilder: ProductDetail.Tmfbuilder?
    var product: ProductDetail?
    var quantity = Float()
    var productId = String()
    var price = Float()
    var totalPrice = Float()
    var optionsTotal = Float()
    var drink = String()
    var type = String()
    var selectedOptions = Dictionary<String, String>()
    var optionsAndPrice = OrderedDictionary<String,String>()
    var typeAndPrice = Dictionary<String, String>()
    let activityIndicatorView = UIActivityIndicatorView()
    var mOptionsModel = OptionsModel()
    var productPriceFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProductDetails()
        navigationItem.rightBarButtonItem = cartBarButtonItem
        quantityButtonsContainerView.layer.cornerRadius = 21.0
        quantityButtonsContainerView.layer.borderColor = UIColor.lightGray.cgColor
        quantityButtonsContainerView.layer.borderWidth = 1.0
        quantityButtonsContainerView.layer.masksToBounds = true
        self.navigationController?.navigationBar.tintColor = UIColor.black
        title = "Meal"
        
        addToCartButton.layer.borderWidth = 2.0
        addToCartButton.layer.cornerRadius = 10.0
        addToCartButton.layer.borderColor = UIColor.white.cgColor
        navigationController?.title = "Cart"
        
        //Alamofire use to dL image
        //            /*(Alamofire.request(imageURL!) as AnyObject).response { (responseData) in
        //                DispatchQueue.main.async {
        //                    if let imageData = responseData.data {
        //                        self.mealImageView.image = UIImage(data: imageData)
        //
        //                    }
        //                }
        //            }*/
        //
        //        }
        
    }
    
    @objc func tapbutton(){
        self.navigationController?.popViewController(animated: true)
        print("tap")
    }
    
    @IBAction func cartAction(_ sender: Any) {
        let vc : CartDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cart") as! CartDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateTotalLabel() {
        totalPrice = ((price * quantity) + optionsTotal)
        totalPrice = Float(Util.formatNumber(totalPrice)!)!
        totalLabel.text = "￡ \(Util.formatNumber(totalPrice) ?? "")"
    }
    
    func getProductDetails() {
        showActivityIndicator()
        Services.getProductById(id: productId, completion: { (productDetail) in
            self.product = productDetail
            self.setData()
            self.hideActivityIndicator()
            self.initializeDictionarise()
        })
    }
    
    func initializeDictionarise(){
        
        var options = [OptionsItemModel]()
        let data = tmfBuilder?.multipleCheckboxesOptionsValue
        
        for (index, _) in (data?.enumerated() ?? [[""]].enumerated()) {
            var itemModel = OptionsItemModel()
            var optionsNewMap = OrderedDictionary<String,String>()
            if (data?[index].count ?? 0) > 0 {
                for (secondIndex, _) in (data![index].enumerated()) {
                    optionsNewMap[(data?[index][secondIndex])!] = tmfBuilder?.multipleCheckboxesOptionsPrice?[index][secondIndex]
                    itemModel.optionsAndPrice = optionsNewMap
                }
                options.append(itemModel)
            }
        }
        mOptionsModel.items = options
        for (key, value) in zip(Array(tmfBuilder?.multipleCheckboxesOptionsValue?.joined() ?? [[""]].joined()), Array(tmfBuilder?.multipleCheckboxesOptionsPrice?.joined() ?? [[""]].joined())) {
            optionsAndPrice[key] = value
        }
        
        for (key, value) in zip(Array(tmfBuilder?.multipleSelectboxOptionsValue?.joined() ?? [[""]].joined()), Array(tmfBuilder?.multipleSelectboxOptionsPrice?.joined() ?? [[""]].joined())) {
            typeAndPrice[key] = value
        }
    }
    
    func setData(){
        self.quantity = 1
        self.mealNameLabel.text = self.product?.name
        self.mealDecriptionLabel.text = self.product?.description?.htmlToAttributedString?.string
        var enumData: ProductDetail.Value?
        if (product != nil){
            if (product?.images?.count ?? 0 > 0){
                self.mealImageView.image = nil
                if let imageURL = URL(string: (product?.images?[0].src ?? "")) {
                    (AF.request(imageURL)).responseData { (responseData) in
                        DispatchQueue.main.async {
                            if let imageData = responseData.data {
                                self.mealImageView.image = UIImage(data: imageData)
                            }
                        }
                    }
                }
            }
            for data in (self.product!.meta_data!) {
                if data.value != nil {
                    enumData = data.value
                    switch enumData {
                    case .valueClass(let valueClass)?:
                        if valueClass.tmfbuilder != nil{
                            self.tmfBuilder = valueClass.tmfbuilder
                            //                        for options in (self.tmfBuilder?.multipleSelectboxOptionsPrice ?? []) {
                            //                            if options.count > 0 && options[0] != ""{
                            //                                self.price =  Float(options[0])!
                            //                            }
                            //                        }
                            for (index, item) in (self.tmfBuilder?.multipleSelectboxOptionsPrice?.enumerated() ?? [[""]].enumerated()) {
                                for price in item {
                                    if price != ""{
                                        self.price =  Float(price)!
                                        break
                                    }
                                }
                            }
                            //self.price = Float((self.tmfBuilder?.multipleSelectboxOptionsPrice?[0][0])!) ?? 0
                            self.totalLabel.text = "￡ \(self.price)"
                            for optionTitle in (self.tmfBuilder?.multipleSelectboxOptionsTitle ?? []) {
                                if optionTitle.count > 0 {
                                    self.typeDropDown.optionArray = optionTitle
                                }
                            }
                            self.typeDropDown.optionArray = (self.tmfBuilder?.multipleSelectboxOptionsTitle?[safe: 0] ?? [])
                            self.drinksDropDown.optionArray = (self.tmfBuilder?.multipleSelectboxOptionsTitle?[safe: 1] ?? [])
                            self.option3DropDown.optionArray = (self.tmfBuilder?.multipleSelectboxOptionsTitle?[safe: 2] ?? [])
                            self.option4DropDown.optionArray = (self.tmfBuilder?.multipleSelectboxOptionsTitle?[safe: 3] ?? [])
                            self.option5DropDown.optionArray = (self.tmfBuilder?.multipleSelectboxOptionsTitle?[safe: 4] ?? [])
                        }
                        break
                    default:
                        break
                    }
                }
            }
        }
        if price == 0 {
            if (product != nil){
                self.productPriceFlag = true
                price = Float((product?.price)!)!
            }else{
            }
        }
        updateTotalLabel()
        setPriceBasedOnType()
    }
    
    func setPriceBasedOnType(){
        if !productPriceFlag {
        typeDropDown.didSelect { (selectedText, index, id) in
            if (self.typeAndPrice[selectedText] == ""){
                self.setPriceBasedOnSize()
            } else{
                if (self.typeAndPrice[selectedText]! as NSString).floatValue != 0 {
                    self.price = (self.typeAndPrice[selectedText]! as NSString).floatValue
                }
            }
            self.type = selectedText
            self.updateTotalLabel()
        }
        }
    }
    
    func updateOptions(index: Int, flag: Bool){
        if mOptionsModel.items.count > 0 && index == 0 && flag{
            let item = mOptionsModel.items[index]
            self.optionsAndPrice = item.optionsAndPrice
        }else if mOptionsModel.items.count > 0 {
            var constantOptions = mOptionsModel.items[0]
            let item = mOptionsModel.items[index + 1]
            for (index, _) in item.optionsAndPrice.enumerated() {
                constantOptions.optionsAndPrice[item.optionsAndPrice[index].key] = item.optionsAndPrice[index].value
            }
            self.optionsAndPrice = constantOptions.optionsAndPrice
        }
    }
    
    func setPriceBasedOnSize(){
        if !productPriceFlag{
        drinksDropDown.didSelect { (selectedText, index, id) in
            if (self.typeAndPrice[selectedText] != "" || self.price == 0) {
                if (self.typeAndPrice[selectedText]! as NSString).floatValue != 0 {
                    self.price = (self.typeAndPrice[selectedText]! as NSString).floatValue
                }
            }else{
                self.setPriceBasedOnType()
            }
            self.drink = selectedText
            self.updateOptions(index: index, flag: false)
            self.updateTotalLabel()
        }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "optionsSegue" {
            if optionsAndPrice.count > 1 {
                let vc = segue.destination as! OptionsViewController
                vc.optionsAndPrice = self.optionsAndPrice
                vc.delegate = self
            } else {
                Util.showAlert(vc: self, title: "Sorry", msg: "You can not customize this order")
            }
        }
    }
    
    @IBAction func addQuantity(_ sender: Any) {
        if quantity < 99 {
            quantity += 1
            quantityLabel.text = String(quantity)
            updateTotalLabel()
        }
    }
    
    @IBAction func decreasedQuantity(_sender: Any) {
        if quantity > 1 {
            quantity -= 1
            quantityLabel.text = String(quantity)
            updateTotalLabel()
        }
    }
    
    @IBAction func addToCart(_ sender: Any) {
        if totalLabel.text == "￡ 0.0"{
            Util.showAlert(vc: self, title: "Error", msg: "Select all item details!")
        }else{
            self.selectedOptions[type] = drink
            let cartItem = CartItem()
            cartItem.productId = self.productId
            cartItem.quantity = Int(self.quantity)
            cartItem.name = self.product?.name ?? ""
            cartItem.originalPrice = Double(price)
            cartItem.price = Double(self.totalPrice)
            cartItem.selectedOptions = self.selectedOptions.JsonString()
            RealmUtil.setCartItem(cartItem: cartItem)
            Util.showAlert(vc: self, title: "Sucess!", msg: "Added to cart successfully")
        }
    }
}

extension MealDetailViewController: SelectedOptionDictionary {
    
    func getSelectedOptions(options: Dictionary<String, String>) {
        selectedOptions = options
        var optionsStr = String()
        for (key, value) in options.sorted(by: { $0.0 < $1.0 }) {
            optionsStr.append(contentsOf: "\(key)\t￡\(value)\n")
            optionsTotal = optionsTotal + (value as NSString).floatValue
        }
        self.optionsPriceLabel.text = "￡\(optionsTotal)"
        updateTotalLabel()
        self.optionsSelectedLabel.text = optionsStr
    }
    
    func showActivityIndicator() {
        activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicatorView.center = view.center
        if #available(iOS 13.0, *) {
            activityIndicatorView.style = .large
        }
        activityIndicatorView.color = .red
        
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }
    
}
