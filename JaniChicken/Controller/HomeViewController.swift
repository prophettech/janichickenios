//
//  HomeViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 30/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit
import ImageSlideshow
import Firebase
import Alamofire

class HomeViewController: UIViewController {
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var orderButton: UIButton!
    var orderStatus = true
    
    let dataSource = [ImageSource(image: UIImage(named: "carousel1")!),ImageSource(image: UIImage(named:"carousel2")!),ImageSource(image: UIImage(named: "carousel3")!),ImageSource(image: UIImage(named: "carousel4")!)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderButton.layer.borderWidth = 2.0
        orderButton.layer.cornerRadius = 10.0
        orderButton.layer.borderColor = UIColor.white.cgColor
        
        slideShow.slideshowInterval = 5.0
        slideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideShow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        slideShow.pageIndicator = pageControl
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideShow.activityIndicator = DefaultActivityIndicator()
        slideShow.setImageInputs(dataSource)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideShow.addGestureRecognizer(recognizer)
        
        initFirebase()
        //getOrderStatus()
    }
    
    func getOrderStatus(){
        Services.getOrderStatus { (response) in
            if (response.ordersOpen == 1){
                self.orderStatus = true
            }
            if (response.ordersOpen == 0){
                self.orderStatus = false
            }
        }
    }
    
    func initFirebase(){
//        InstanceID.instanceID().instanceID { (result, error) in
//          if let error = error {
//            print("Error fetching remote instance ID: \(error)")
//          } else if let result = result {
//            print("Remote instance ID token: \(result.token)")
//            let uuid = UUID().uuidString
//            let userDefault = UserDefaults.standard
//            //if (!(userDefault.bool(forKey: "updatedToken")))
//
//            let parameters = [
//                "id": uuid,
//                "fcmtoken": result.token
//            ]
//            AF.request(Services.Endpoints.insertToken.url, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
//                switch response.result {
//                case .success:
//                    break
//                    //if let value = response.result {
//                     //   print(value)
//                   // }
//                case .failure(let error):
//                    print(error)
//                }
//            }
////            Services.insertFirebaseToken(id: uuid, token: result.token) { (response) in
////                if response.success == 1  {
////                    userDefault.set(true, forKey: "updatedToken")
////                    userDefault.synchronize()
////                }
////            }
//            }
//          }
    }
    
    @IBAction func orderAction(_ sender: Any) {
        if (orderStatus){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "NavVC") as! UINavigationController
            show(viewController, sender: self)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "orderClosed") as! ClosedOrderViewController
            show(viewController, sender: self)
        }
    }
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @objc func didTap() {
        let fullScreenController = slideShow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
}
