//
//  CartViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit
import iOSDropDown
import Stripe
import Alamofire

class CartViewController : UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cartTotalLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var placeOrderButton: UIButton!
    @IBOutlet weak var addressFieldView: UIView!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var postCodeTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var notesTextField: UITextField!
    @IBOutlet weak var paymentTypeDropDown: DropDown!
    @IBOutlet weak var totalQuantityLabel: UILabel!
    @IBOutlet weak var shippingTypeDropDown: DropDown!
    
    let activityIndicatorView = UIActivityIndicatorView()
    let paymentOptions = ["Pay by Cash", "Pay by Card (Stripe)"]
    let shippingOptions = ["Delivery", "Collection"]
    var cash = false
    var delivery = false
    var validPostCode = false
    var exist = false
    var total = 0.0
    var deliveryCharge = "1.5"
    var cart = Cart()
    var order: PlaceOrder?
    
    @IBOutlet weak var cartTextField: STPPaymentCardTextField!
    @IBOutlet weak var cardTextFieldHeight: NSLayoutConstraint!
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    var paymentInProgress: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                if self.paymentInProgress {
                    self.activityIndicator.startAnimating()
                    self.activityIndicator.color = .red
                    self.activityIndicator.alpha = 1
                    self.placeOrderButton.alpha = 0
                } else {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.alpha = 0
                    self.placeOrderButton.alpha = 1
                }
            }, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placeOrderButton.layer.borderWidth = 2.0
        placeOrderButton.layer.cornerRadius = 10.0
        placeOrderButton.layer.borderColor = UIColor.white.cgColor
        
        postCodeTextField.addTarget(self, action: #selector(CartViewController.textFieldDidChange(_:)),for: .editingChanged)
        postCodeTextField.addTarget(self, action: #selector(CartViewController.editingEnd(_:)), for: .editingDidEnd)
        
        tableView.dataSource = self
        RealmUtil.getCartItems { (items) in
            if (items.products.count < 0){
                self.toggleEmptyCart(isHidden: true)
                // tehre's no item here.
                // if there's no item in cart, tell the user to do some browsing
                let emptyCartLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80))
                emptyCartLabel.center = self.view.center
                emptyCartLabel.textAlignment = .center
                emptyCartLabel.text = "Your cart is empty.\n Please add some meals."
                emptyCartLabel.numberOfLines = 3
                
                self.view.addSubview(emptyCartLabel)
            } else {
                // display all the items in cart
                self.cart = items
                self.paymentTypeDropDown.optionArray = self.paymentOptions
                self.shippingTypeDropDown.optionArray = self.shippingOptions
                self.toggleEmptyCart(isHidden: false)
                self.getMealsInCart()
                self.total = items.totalPrice
                self.totalQuantityLabel.text = String(items.totalQuantity)
                self.cartTotalLabel.text = "￡" + Util.formatNumber(Float(self.total))!
            }
        }
        paymentTypeDropDown.didSelect { (selectedText, index, id) in
            switch(selectedText){
            case "Pay by Cash":
                self.cash = true
                self.cash = true
                self.setupPaymentTextField()
                self.cardTextFieldHeight.constant = 0
                break
            case "Pay by Card (Stripe)":
                self.cash = false
                self.setupPaymentTextField()
                self.cardTextFieldHeight.constant = 60
                break
            default:
                break
            }
        }
        shippingTypeDropDown.didSelect { (selectedText, index, id) in
            switch(selectedText){
            case "Collection":
                self.delivery = false
                self.updateDeliveryCharge()
                break
            case "Delivery":
                self.delivery = true
                self.updateDeliveryCharge()
                break
            default:
                break
            }
        }
    }
    
    func setupPaymentTextField() {
        cartTextField.isHidden = cash
    }
    
    func updateDeliveryCharge(){
        if delivery {
            self.cartTotalLabel.text = "￡" + Util.formatNumber(Float(self.total))! + " + ￡" + deliveryCharge + " Delivery"
        }else{
            self.cartTotalLabel.text = "￡" + Util.formatNumber(Float(self.total))!
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (Util.isValidPostCode(testStr: textField.text ?? "")){
            validPostCode = true
            verifyPostCode(postCode: textField.text!)
        }else{
            print("Invalid postcode")
            validPostCode = false
        }
    }
    
    @objc func editingEnd(_ textField: UITextField){
        if (!Util.isValidPostCode(testStr: textField.text ?? "") || !exist){
            Util.showAlert(vc: self, title: "Error", msg: "Invalid PostCode or We do not deliver to this post code.")
        }
    }
    
    func toggleEmptyCart(isHidden: Bool) {
        tableView.isHidden = isHidden
        addressFieldView.isHidden = isHidden
        placeOrderButton.isHidden = isHidden
    }
    
    func getMealsInCart() {
        tableView.reloadData()
        cartTotalLabel.text = ""
    }
    
    
    @IBAction func placeOrder()
    {
        if validateBillingForm(){
            paymentInProgress = true
            var lineItems = [LineItem]()
            
            for cartItem in cart.products {
                let lineItem = LineItem(name: cartItem.name + cartItem.selectedOptions, productID: Int(cartItem.productId), variationID: nil, quantity: Int(cartItem.quantity), taxClass: nil, subtotal: nil, subtotalTax: nil, total: String(cartItem.price), totalTax: nil, taxes: nil, metaData: nil, sku: nil, price: Int(cartItem.price))
                lineItems.append(lineItem)
            }
            
            let billing = Ing(firstName: firstNameTextField.text, lastName: lastNameTextField.text, address1: address1TextField.text, address2: address2TextField.text, city: cityTextField.text, state: "" , postcode: postCodeTextField.text, country: "UK", email: emailTextField.text, phone: phoneTextField.text)
            
            let deliveryShipping = ShippingLines(methodID: "flat_rate", methodTitle: "Delivery", total: "1")
            
            let collectionShipping = ShippingLines(methodID: "flat_rate", methodTitle: "Collection", total: "0")
            
            if cash {
                 if delivery {
                     let order = PlaceOrder(paymentMethod: "Cash", paymentMethodTitle: "Cash", setPaid: true, billing: billing, shipping: billing, lineItems: lineItems, shippingLines: [deliveryShipping])
                     placeOrderOnWordpress(order: order)
                 }else{
                     let order = PlaceOrder(paymentMethod: "Cash", paymentMethodTitle: "Cash", setPaid: true, billing: billing, shipping: billing, lineItems: lineItems, shippingLines: [collectionShipping])
                     placeOrderOnWordpress(order: order)
                 }
             }else {
                 if delivery {
                     let order = PlaceOrder(paymentMethod: "Pay with Stripe", paymentMethodTitle: "Stripe", setPaid: true, billing: billing, shipping: billing, lineItems: lineItems, shippingLines: [deliveryShipping])
                     self.order = order
                     payViaStripe(order: order)
                 }else{
                     let order = PlaceOrder(paymentMethod: "Pay with Stripe", paymentMethodTitle: "Stripe", setPaid: true, billing: billing, shipping: billing, lineItems: lineItems, shippingLines: [collectionShipping])
                     self.order = order
                     payViaStripe(order: order)
                 }
             }
        }
    }
    
    func payViaStripe(order: PlaceOrder){
        
        MyAPIClient.createPaymentIntent(amount:((total + Double(deliveryCharge)!) * 100).rounded(toPlaces: 0),currency:"gbp") { (paymentIntentResponse) in
            switch paymentIntentResponse {
            case .success(let clientSecret):
                
                print(clientSecret)
                let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
                
                let paymentBillingDetails = STPPaymentMethodBillingDetails()
                paymentBillingDetails.name = (order.billing?.firstName)! + " " + (order.billing?.lastName)!
                paymentBillingDetails.phone = order.billing?.phone
                paymentBillingDetails.email = order.billing?.email
                
                let paymentMethodparams = STPPaymentMethodParams(card: self.cartTextField
                    .cardParams, billingDetails: paymentBillingDetails, metadata: nil)
                paymentIntentParams.paymentMethodParams = paymentMethodparams
                
                
                STPPaymentHandler.shared().confirmPayment(withParams: paymentIntentParams, authenticationContext: self) { (status, paymentIntent, error) in
                    self.paymentInProgress = false
                    var resultString = ""
                    switch (status) {
                    case .canceled:
                        resultString = "Payment canceled"
                        let alertController = UIAlertController(title: "Error", message: resultString, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    case .failed:
                        resultString = "Payment failed, please try a different card"
                        resultString = "Payment canceled"
                        let alertController = UIAlertController(title: "Error", message: resultString, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    case .succeeded:
                        resultString = "Payment successful, Please wait on This Screen for Confirmation!!!"
                        let alertController = UIAlertController(title: "Success", message: resultString, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default){ (action: UIAlertAction) in
                            self.placeOrderOnWordpress(order: self.order!)
                        }
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                    print(resultString)
                }
            case .failure(let error):
                print(error)
                self.paymentInProgress = false
                Util.showAlert(vc: self, title: "Error", msg: "Payment Failed!")
                break
            }
        }
    }
    
    func placeOrderOnWordpress(order: PlaceOrder){
        showActivityIndicator()
        Services.placeOrder(body: order) { (response) in
            self.hideActivityIndicator()
            if response != nil {
                if response?.id != nil{
                    self.addOrderNotes(order: response!)
                }
            }
        }
    }
    
    func addOrderNotes(order: OrderResponse){
        var note = String()
        
        for item in cart.products {
            note.append(item.selectedOptions.description)
        }
        
        let body = NotesBody(note: note)
        
        Services.addNotes(id: order.id!, body: body) { (response) in
            let vc : TrackOrderViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrackVC") as! TrackOrderViewController
            vc.order = order
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func verifyPostCode(postCode: String){
        exist = false
        deliveryCharge = "1.0"
        let postCodes = PostCodes()
        
        for code in postCodes.area1 {
            if PostCodes.postcodeInRange(regex: code, postCode: postCode){
                deliveryCharge = "1.0"
                exist = true
            }
        }
    
        for code in postCodes.area2 {
            if PostCodes.postcodeInRange(regex: code, postCode: postCode){
                deliveryCharge = "1.5"
                exist = true
            }
        }
        
        for code in postCodes.area2 {
            if PostCodes.postcodeInRange(regex: code, postCode: postCode){
                deliveryCharge = "2.5"
                exist = true
            }
        }

        for code in postCodes.area2 {
            if PostCodes.postcodeInRange(regex: code, postCode: postCode){
                deliveryCharge = "2.0"
                exist = true
            }
        }

    }
}


extension CartViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as! CartItemCell
        cell.cartItem =  cart.products[indexPath.row]
        return cell
    }
    
    func validateBillingForm() -> Bool{
        if firstNameTextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your first name")
            return false
        }
        if lastNameTextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your last name")
            return false
        }
        if address1TextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your address")
            return false
        }
        if cityTextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your city")
            return false
        }
        if postCodeTextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your postcode")
            return false
        }
        if phoneTextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your phone number")
            return false
        }
        if emailTextField.text == "" {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter your email")
            return false
        }
        
        if Util.isValidEmail(testStr: emailTextField.text!) {
            return true
        }else {
            Util.showAlert(vc: self, title: "Error", msg: "Please enter valid email")
            return false
        }
    }
    
    func showActivityIndicator()
    {
        activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicatorView.center = tableView.center
        if #available(iOS 13.0, *) {
            activityIndicatorView.style = .large
        }
        activityIndicatorView.color = .red
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator()
    {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }
}


extension CartViewController: STPAuthenticationContext {
    
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}
