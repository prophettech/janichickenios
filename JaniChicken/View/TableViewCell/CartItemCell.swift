//
//  CartItemCell.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation
import UIKit

class CartItemCell: UITableViewCell {
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealSubtotalLabel: UILabel!
    @IBOutlet weak var productIdLabel: UILabel!
        
    var cartItem: CartItem! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI() {
        mealNameLabel.text = cartItem.name
        quantityLabel.text = String(cartItem.quantity)
        productIdLabel.text = "#" + cartItem.productId
        mealSubtotalLabel.text = "￡" + Util.formatNumber(Float(cartItem!.price))!
    }
}
