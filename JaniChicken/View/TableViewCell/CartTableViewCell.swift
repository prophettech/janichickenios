//
//  CartTableViewCell.swift
//  Food Delivery
//
//  Created by Akram Hussain on 09/01/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import UIKit
import Lottie

protocol CartItemsDelegate {
    func onAddItem(item: CartItem)
    func onRemoveItem(item: CartItem)
}

class CartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantity2Label: UILabel!
    @IBOutlet weak var quantityButtonContainerView: UIView!
    @IBOutlet weak var animaitonView: AnimationView!
    
    var delegate: CartItemsDelegate?
    var cartItem: CartItem! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI() {
        quantityButtonContainerView.layer.cornerRadius = 21.0
        quantityButtonContainerView.layer.borderColor = UIColor.lightGray.cgColor
        quantityButtonContainerView.layer.borderWidth = 1.0
        quantityButtonContainerView.layer.masksToBounds = true
        
        NameLabel.text = cartItem.name
        idLabel.text = "#" + cartItem.productId
        quantityLabel.text = String(cartItem.quantity)
        priceLabel.text = "￡" + Util.formatNumber(Float(cartItem!.price))!
        quantity2Label.text = String(cartItem.quantity)
        
        animaitonView.animation = Animation.named("food_carousel")
        animaitonView.play()
        animaitonView.loopMode = .loop

    }
    
    @IBAction func addAction(_ sender: Any) {
        delegate?.onAddItem(item: cartItem)
    }
    
    @IBAction func removeAction(_ sender: Any) {
        delegate?.onRemoveItem(item: cartItem)
    }
    
}
