//
//  Util.swift
//  Food Delivery
//
//  Created by Akram Hussain on 06/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation
import UIKit

class Util{
    
    class func showAlert (vc: UIViewController,title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }

    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func isValidPostCode(testStr:String) -> Bool {
        let emailRegEx = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func formatNumber(_ number: Float) -> String? {

       let formatter = NumberFormatter()
       formatter.minimumFractionDigits = 2 // minimum number of fraction digits on right
       formatter.maximumFractionDigits = 2 // maximum number of fraction digits on right, or comment for all available
       formatter.minimumIntegerDigits = 1 // minimum number of integer digits on left (necessary so that 0.5 don't return .500)

        let formattedNumber = formatter.string(from: (NSNumber.init(value: number)))
        return formattedNumber
    }
}

extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension String{
func dictionaryValue() -> [String: AnyObject]
{
    if let data = self.data(using: String.Encoding.utf8) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject]
            return json!

        } catch {
            print("Error converting to JSON")
        }
    }
    return NSDictionary() as! [String : AnyObject]
}
}

extension Dictionary{
    func JsonString() -> String
    {
        do{
            let jsonData: Data
            if #available(iOS 13.0, *) {
                jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted, .withoutEscapingSlashes])
            } else {
                jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
            }
        return String.init(data: jsonData, encoding: .utf8)!
        }
        catch
        {
            return "error converting"
        }
    }
}

class PostCodes{
 
    let area1 = ["NE6(?:.*)"]
    
    let area2 = ["NE21(?:.*)"]
    
    let area3 = ["NE22(?:.*)"]

    let area4 = ["NE77(?:.*)", "NE28(?:.*)"]


    class func postcodeInRange(regex:String, postCode: String) -> Bool {
         
         let postCodePred = NSPredicate(format:"SELF MATCHES %@", regex)
         return postCodePred.evaluate(with: postCode)
     }
}
