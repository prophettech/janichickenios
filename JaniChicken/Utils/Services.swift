//
//  Services.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

class Services {
    
    static let consumerSecret = "cs_7c9c28a3b0774ed27d85666b22e3b19a8b38ef18"
    static let consumerKey = "ck_ff8ef3c827bed02038a1c6a159e7280a322d774a"
    
    
    enum Endpoints {
        static let base = "https://janichicken.co.uk/"
        
        case getAllProducts
        case getAllCategories
        case getProductsByCategory
        case getProductById
        case placeOrder
        case makePayment
        case addNote
        case orderStatus
        case insertToken
        
        var stringValue: String {
            switch self {
                
            case .getAllProducts: return Endpoints.base + "wp-json/wc/v3/products"
                + "?consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
                
            case .getAllCategories: return Endpoints.base + "wp-json/wc/v3/products/categories"
                + "?per_page=100&consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
                
            case .getProductsByCategory: return Endpoints.base + "wp-json/wc/v3/products?per_page=100&category="
                
            case .getProductById: return Endpoints.base + "wp-json/wc/v3/products/"
                
            case .placeOrder: return Endpoints.base + "wp-json/wc/v3/orders"
                + "?consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
                
            case .makePayment: return "https://api.worldpay.com/v1/orders"
                
            case .addNote: return Endpoints.base + "wp-json/wc/v3/orders/"
            
            case .orderStatus: return Endpoints.base + "fcm/orders.json"
            
            case .insertToken: return Endpoints.base + "fcm/inserttoken.php"
                    
            }
        }
        
        var url: URL {
            return URL(string: stringValue)!
        }
    }
        
    class func getAllProducts(completion: @escaping (Product?) -> Void) {
        _ = taskForGETRequest(url: Endpoints.getAllProducts.url, responseType: Product.self) { response, error in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getAllCategories(completion: @escaping (Categories?) -> Void){
        _ = taskForGETRequest(url: Endpoints.getAllCategories.url, responseType: Categories.self) { response, error in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getProductsByCategory(id: String, completion: @escaping (Products?) -> Void){
        let urlValue = Endpoints.getProductsByCategory.stringValue + id + "&consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
        
        guard let url = URL(string: urlValue) else { return }
        
        _ = taskForGETRequest(url: url, responseType: Products.self) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getProductById(id: String, completion: @escaping (ProductDetail?) -> Void){
        let urlValue = Endpoints.getProductById.stringValue + id + "?consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
        
        guard let url = URL(string: urlValue) else { return }
        
        _ = taskForGETRequest(url: url, responseType: ProductDetail.self) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func addNotes(id: Int, body: NotesBody, completion: @escaping(NotesResponse?) -> Void) {
        let urlValue = Endpoints.addNote.stringValue + String(id) + "/notes" + "?consumer_key=\(Services.consumerKey)" +
        "&consumer_secret=\(Services.consumerSecret)"
        
        guard let url = URL(string: urlValue) else { return }
        
        taskForPOSTRequest(url: url, responseType: NotesResponse.self, body: body) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
            
        }
    }
    
    class func placeOrder(body: PlaceOrder, completion: @escaping (OrderResponse?) -> Void){
        taskForPOSTRequest(url: Endpoints.placeOrder.url, responseType: OrderResponse.self, body: body) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func makePayment(body: MakePayment, completion: @escaping(WorlpayResponse?) -> Void){
        let url = URL(string: "https://api.worldpay.com/v1/orders")
   
    }
    
    class func getOrderStatus(completion: @escaping(OrderStatusRespones) -> Void){
        _ = taskForGETRequest(url: Endpoints.orderStatus.url, responseType: OrderStatusRespones.self) { (response, error) in
            if let response = response {
                completion(response)
            }
        }
    }
    
    class func insertFirebaseToken(id: String,token: String, completion: @escaping(FirebaseTokenResponse) -> Void){
        let body = FirebaseToken(id: id, fcmtoken: token)
        taskForPOSTRequest(url: Endpoints.insertToken.url, responseType: FirebaseTokenResponse.self, body: body) { (response, error) in
            if let response = response{
                completion(response)
            }
        }
    }
    
    class func taskForGETRequest<ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, completion: @escaping (ResponseType?, Error?) -> Void) -> URLSessionDataTask {
        var request = URLRequest(url: url)
        request.addValue("query_string_auth", forHTTPHeaderField: "true")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            print("---*****-----", String(decoding: data!, as: UTF8.self))
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            let decoder = JSONDecoder()
            do {
                let responseObject = try decoder.decode(ResponseType.self, from: data)
                print("--------", responseObject)
                DispatchQueue.main.async {
                    completion(responseObject, nil)
                }
            } catch {
                do {
                    let errorResponse = try decoder.decode(Response.self, from: data) as Error
                    DispatchQueue.main.async {
                        completion(nil, errorResponse)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        }
        task.resume()
        
        return task
    }
    
    class func taskForPOSTRequest<RequestType: Encodable, ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, body: RequestType, completion: @escaping (ResponseType?, Error?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try! JSONEncoder().encode(body)
        print("--------", String(decoding: request.httpBody!, as: UTF8.self))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            let decoder = JSONDecoder()
            do {
                let responseObject = try decoder.decode(ResponseType.self, from: data)
                DispatchQueue.main.async {
                    completion(responseObject, nil)
                }
            } catch {
                do {
                    let errorResponse = try decoder.decode(Response.self, from: data) as Error
                    DispatchQueue.main.async {
                        completion(nil, errorResponse)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        }
        task.resume()
    }
    
}
